resource "azurerm_virtual_network" "demo-vnet" {
  name                = "demo-vnet"
  location            = "East US"
  address_space       = ["10.0.0.0/24"]
  resource_group_name = "demo"

  subnet {
    name           = "public"
    address_prefix = "10.0.0.0/24"
  }
}
